<?php

namespace App\Http\Controllers;

use App\Http\Requests\VenteBilletRequest;
use App\Models\Billet;
use App\Models\Lieu;
use App\Models\Pack;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BilletController extends Controller
{

    public function goToVenteBillet(){
        $listePack = Pack::all();
        $listeLieu = Lieu::all();
        $title = "vente";
        return view('billet.venteBillet',compact(['listePack','listeLieu','title']));
    }

    public function venteBillet(VenteBilletRequest $request){
        $billet = new Billet();
        $credentials = $request->validated();
        $etudiant = Auth::guard('etudiantGuard')->id();
        $billet->createBillet($credentials['pack'],$credentials['lieu'],$credentials['nombre'],$etudiant);
        return to_route('billet.toVente');
    }

    public function billetVendu(){
        $etudiant = Auth::guard('etudiantGuard')->id();
        $listeBilletVendu = DB::table('v_billetvendu')->where('etudiant',$etudiant)->get();
        $title = "billet vendu";
        return view('billet.billetVendu',compact(['listeBilletVendu','title']));
    }

    public function coutDeRevientBillet(){
        $liste = DB::table('v_depensepack')->get();
        $title = "cout de revient billet";
        return view('billet.coutDeRevientBillet',compact(['liste','title']));
    }

}
