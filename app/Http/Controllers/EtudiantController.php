<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\etudiant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class EtudiantController extends Controller
{
    public  function createEtudiant(){
        etudiant::create([
            'nom_etudiant' => 'thony',
            'profil' => 1,
            'email' => 'thony@gmail.com',
            'password' => Hash::make('0000') 
        ]);
    }

    public function login(LoginRequest $request){
        $credentials = $request->validated();
        if(Auth::guard('etudiantGuard')->attempt($credentials)){
            $request->session()->regenerate();
            $title = "accueil";
            return view('welcome',compact('title'));
         }
         return redirect()->back()->withErrors([
            'errorLogin' => 'Invalide email ou mot de passe.'
         ])->onlyInput('email');
    }

    public function logOut(){
        Auth::guard('etudiantGuard')->logout();
        return view('authentification.login');
    }
}
