<?php

namespace App\Http\Controllers;

use App\Http\Requests\PackRequest;
use App\Models\Pack;
use Illuminate\Http\Request;

class PackController extends Controller
{
    public function createPack(PackRequest $request){
        $validatedData = $request->validated(); 
        Pack::create([
            'nom_pack'=>$validatedData['nom_pack'],
            'prix_pack'=>$validatedData['prix_pack']
        ]);
        return to_route('pack.goToCreationPack');
    }

    public function goToCreationPack(){
        $title  ="creation pack";
        return view('pack.createPack',compact('title'));
    }
}
