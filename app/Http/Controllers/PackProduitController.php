<?php

namespace App\Http\Controllers;

use App\Http\Requests\PackProduitRequest;
use App\Models\Pack;
use App\Models\PackProduit;
use App\Models\Produit;
use Illuminate\Http\Request;

class PackProduitController extends Controller
{
    public function gotToCreateFormule(){
        $listePack = Pack::all();
        $listeproduit = Produit::all();
        $title = "pack formule";
        return view('pack.formulePack',compact(['listePack','listeproduit','title']));
    }


    public function createFormulePack(PackProduitRequest $request){
        $validatedData = $request->validated(); 
        $produits = $validatedData['produits'];
        $pack = $validatedData['pack'];
        $quantite = $validatedData['quantite'];
        $i=0;
        foreach ($produits as $key => $value) {
            PackProduit::insert([
                'produit'=>$value,
                'pack'=>$pack,
                'quantite'=>$quantite[$i]
            ]);
            $i++;
        }
        return to_route('pack.formulePack');
    }
}
