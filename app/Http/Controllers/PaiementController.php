<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaiementRequest;
use App\Models\Client;
use App\Models\Paiement;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PaiementController extends Controller
{
    public function goToPaiement(){
        $listeClient = Client::all();
        $title = "paiement";
        return view('billet.paiement',compact(['title','listeClient']));
    }

    public function doPaiement(PaiementRequest $request){
        $validatedData = $request->validated(); 
        $paiement = new Paiement();
        try{
            $paiement->doPaiement($validatedData['numero'],$validatedData['client'],$validatedData['montant']);
            return to_route('paiement.toPaiement');
        }catch (Exception $e) {
            return to_route('paiement.toPaiement')->withErrors([
                'error' => $e->getMessage()
             ]);
        }
    }

    public function etatPaiementBillet(){
        $etudiant = Auth::guard('etudiantGuard')->id();
        $listeBillet = DB::table('v_etatpaiement')->where('etudiant', $etudiant)->get();
        $title="etat paiement";
        return view('billet.etatPaiement',compact(['title','listeBillet']));
    }

    public function billetRestantPayer(){
        $etudiant = Auth::guard('etudiantGuard')->id();
        $listeBillet = DB::table('v_billetnonpayer')->where('etudiant', $etudiant)->get();
        $title="reste a payer";
        return view('billet.resteAPayer',compact(['title','listeBillet']));
    } 

}
