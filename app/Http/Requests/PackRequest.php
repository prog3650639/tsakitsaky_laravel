<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'nom_pack'=>'required',
            'prix_pack'=>'required|min:1'
        ];
    }

    public function messages(): array
    {
        return [
            'nom_pack.required' => 'Le champ nom est obligatoire.',
            'prix_pack.required' => 'Le champ prix est obligatoire.',
            'prix_pack.min' => 'Le prix doit être supérieur à 1.',
        ];
    }

}
