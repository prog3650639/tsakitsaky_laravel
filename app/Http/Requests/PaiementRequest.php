<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaiementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'numero'=>'required',
            'client'=>'required',
            'montant'=>'required|min:1'
        ];
    }

    public function messages(): array
    {
        return [
            'numero.required' => 'Le champ numero est obligatoire.',
            'client.required' => 'Le champ client est obligatoire.',
            'montant.required' => 'Le champ montant est obligatoire.',
            'montant.min' => 'Le monant doit être supérieur à 1.',
        ];
    }
}
