<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VenteBilletRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'pack'=>'required',
            'lieu'=>'required',
            'nombre'=>'required|min:1'
        ];
    }

    /**
     * Custom error messages for validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'pack.required' => 'Le champ pack est obligatoire.',
            'lieu.required' => 'Le champ lieu est obligatoire.',
            'nombre.required' => 'Le champ nombre est obligatoire.',
            'nombre.min' => 'Le nombre doit être supérieur à :min.',
        ];
    }
}
