<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Billet extends Model
{
    use HasFactory;

    protected $table = 'billet';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    protected $fillable = ['etudiant','pack','lieu','etatpaiement','reste_a_payer'];
    public $timestamps = false; 


    public function createBillet($pack,$lieu,$nombre,$etudiant){
        $prixPack = Pack::where('id', $pack)->value('prix_pack');
        for($i=0;$i<$nombre;$i++){
            self::create([
                'etudiant'=>$etudiant,
                'pack'=>$pack,
                'lieu'=>$lieu,
                'etatpaiement'=>0,
                'reste_a_payer'=>$prixPack
            ]);
        }
    }

    public function billetVendu(){
        $listeBilletVendu = DB::table('billetVendu')->get();
    }
}
