<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pack extends Model
{
    use HasFactory;

    protected $table = 'pack';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    protected $fillable = ['nom_pack','prix_pack'];
    public $timestamps = false; 
}
