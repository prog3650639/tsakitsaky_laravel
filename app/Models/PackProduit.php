<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackProduit extends Model
{
    use HasFactory;

    protected $table = 'packproduit';
    protected $fillable = [ 'produit','pack','quantite'];
    public $timestamps = false; 
}
