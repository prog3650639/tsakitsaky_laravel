<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Paiement extends Model
{
    use HasFactory;

    protected $table = 'paiement';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    protected $fillable = ['billet','client','montant'];
    public $timestamps = false; 

    public function DoPaiement($billet,$client,$montant){
        $ifExistBillet = Billet::where('id',$billet)->exists();
        try{
            if($ifExistBillet==false){
                throw new Exception("le billet ".$billet." n'existe pas");
            }
            $billetComplet = DB::table('v_billet_complet')
                    ->select('prix_pack', 'reste_a_payer')
                    ->where('id', $billet)
                    ->first();
            $prix = $billetComplet->prix_pack;
            if($montant>$prix){
                throw new Exception("Montant supérieur au prix du pack");
            }
            $resteAPayer = $billetComplet->reste_a_payer-$montant;
            self::create([
                'billet'=>$billet,
                'client'=>$client,
                'montant'=>$montant
            ]);

            if($resteAPayer>0){
                Billet::where('id',$billet)->update(['reste_a_payer' => $resteAPayer]);
            }else if($resteAPayer==0){
                Billet::where('id',$billet)->update(['reste_a_payer' => $resteAPayer,'etatpaiement'=>1]);
            }
        }catch (Exception $e) {
            throw $e;
        }
    }
}
