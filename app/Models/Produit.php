<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    use HasFactory;

    protected $table = 'produit';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    protected $fillable = ['nom_produit','cout_de_revient','quantite_unite','unite'];
    public $timestamps = false; 

}
