INSERT INTO pack(nom_pack,prix_pack) 
VALUES 
('billet 20 000 Ar',20000),
('billet 30 000 Ar',30000)
;

INSERT INTO axe(nom_axe) 
VALUES
('axe1')
; 

INSERT INTO axe(nom_axe) 
VALUES
('axe2')
; 

INSERT INTO lieu(nom_lieu,axe) 
VALUES
('tanjombato','axe1'),
('andoharanofotsy','axe1'),
('iavoloha','axe1'),
('Anosy','axe2'),
('Analakely','axe2'),
('Ambohijatovo','axe2')
;


INSERT INTO unite(nom_unite) 
VALUES
('g'),
('piece')
; 


INSERT INTO produit(nom_produit,cout_de_revient,quantite_unite,unite)
VALUES 
('tsatsiou',10000,100,'unite1'),
('beignet de crevette',300,1,'unite2'),
('emballage',1000,1,'unite2')
;

INSERT INTO packProduit VALUES 
('produit4','pack1',40),
('produit6','pack1',1),
('produit5','pack2',6),
('produit6','pack2',1)
;

INSERT INTO client(nom,contact) VALUES 
('bad','0345678900'),
('good','0345678905')
;