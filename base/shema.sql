CREATE TABLE etudiant(
    id VARCHAR(50) DEFAULT 'etudiant' || nextval('etudiantSequence')::TEXT PRIMARY KEY,
    nom_etudiant VARCHAR(50),
    profil INTEGER,
    email VARCHAR(50),
    password VARCHAR(50)
);

CREATE TABLE unite (
    id VARCHAR(50) DEFAULT 'unite' || nextval('uniteSequence')::TEXT PRIMARY KEY,
    nom_unite VARCHAR(50)
);

CREATE TABLE produit(
    id VARCHAR(50) DEFAULT 'produit' || nextval('produitSequence')::TEXT PRIMARY KEY,
    nom_produit VARCHAR(50),
    cout_de_revient DOUBLE PRECISION,
    quantite_unite DOUBLE PRECISION,
    unite VARCHAR(50),
    FOREIGN KEY(unite) REFERENCES unite(id)
);

CREATE TABLE pack(
    id VARCHAR(50) DEFAULT 'pack' || nextval('packSequence')::TEXT PRIMARY KEY,
    nom_pack VARCHAR(50),
    prix_pack DOUBLE PRECISION
);

CREATE TABLE imagePack(
    pack VARCHAR(50),
    nom_image VARCHAR(50),
    FOREIGN KEY(pack) REFERENCES pack(id)
);

CREATE TABLE packProduit(
    produit VARCHAR(50),
    pack VARCHAR(50),
    quantite DOUBLE PRECISION,
    FOREIGN KEY(produit) REFERENCES produit(id),
    FOREIGN KEY(pack) REFERENCES pack(id)
);

CREATE TABLE axe (
    id VARCHAR(50) DEFAULT 'axe' || nextval('axeSequence')::TEXT PRIMARY KEY,
    nom_axe VARCHAR(50)
);


CREATE TABLE lieu (
    id VARCHAR(50) DEFAULT 'lieu' || nextval('lieuSequence')::TEXT PRIMARY KEY,
    nom_lieu VARCHAR(50),
    axe VARCHAR(50),
    FOREIGN KEY(axe) REFERENCES axe(id)
);


CREATE TABLE client(
    id VARCHAR(50) DEFAULT 'client' || nextval('clientSequence')::TEXT PRIMARY KEY,
    nom VARCHAR(50),
    contact VARCHAR(50)
);


CREATE TABLE billet(
    id VARCHAR(50) DEFAULT 'billet' || nextval('billetSequence')::TEXT PRIMARY KEY,
    etudiant VARCHAR(50),
    pack VARCHAR(50),
    lieu VARCHAR(50),
    etatPaiement INT,
    FOREIGN KEY(etudiant) REFERENCES etudiant(id),
    FOREIGN KEY(pack) REFERENCES pack(id),
    FOREIGN KEY(lieu) REFERENCES lieu(id)
);
ALTER TABLE billet ADD COLUMN reste_a_payer DOUBLE PRECISION;


CREATE TABLE paiement(
    id VARCHAR(50) DEFAULT 'paiement' || nextval('paiementSequence')::TEXT PRIMARY KEY,
    billet VARCHAR(50),
    client VARCHAR(50),
    FOREIGN KEY(billet) REFERENCES billet(id),
    FOREIGN KEY(client) REFERENCES client(id)
);
ALTER TABLE paiement ADD COLUMN montant DOUBLE PRECISION;
