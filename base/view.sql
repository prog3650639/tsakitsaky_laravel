CREATE OR REPLACE VIEW v_billetVendu AS
SELECT etudiant,COUNT(*) as nombre,pack.nom_pack
FROM billet
JOIN pack ON pack.id = billet.pack
WHERE etatPaiement = 1
GROUP BY etudiant,pack,pack.nom_pack
;



CREATE OR REPLACE VIEW v_depensePack AS 
SELECT 
pack.nom_pack,SUM((packProduit.quantite*produit.cout_de_revient)/produit.quantite_unite ) as montant
FROM packProduit
JOIN produit ON packProduit.produit = produit.id
JOIN pack ON packProduit.pack = pack.id
GROUP BY packProduit.pack,pack.nom_pack
;


CREATE OR REPLACE VIEW v_billte_lib AS 
SELECT etudiant.nom_etudiant,pack.nom_pack,pack.prix_pack,lieu.nom_lieu,billet.etatpaiement,billet.reste_a_payer
FROM billet
JOIN etudiant ON billet.etudiant = etudiant.id
JOIN pack ON billet.pack = pack.id
JOIN lieu ON billet.lieu = lieu.id
;


CREATE OR REPLACE VIEW v_billet_complet AS 
SELECT 
    billet.id,billet.etudiant,etudiant.nom_etudiant,billet.pack,pack.nom_pack,pack.prix_pack,billet.lieu,lieu.nom_lieu,billet.etatpaiement,billet.reste_a_payer
FROM billet
JOIN etudiant ON billet.etudiant = etudiant.id
JOIN pack ON billet.pack = pack.id
JOIN lieu ON billet.lieu = lieu.id
;



CREATE OR REPLACE VIEW  v_etatpaiement AS
SELECT 
    id,nom_pack,etudiant,(prix_pack-reste_a_payer) as montantpayer 
FROM v_billet_complet
;


CREATE OR REPLACE VIEW v_billetnonpayer AS 
SELECT 
    id,nom_pack,reste_a_payer ,etudiant
FROM v_billet_complet 
WHERE reste_a_payer!=0
;