@extends('welcome')

@section('content')

<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                <h4 class="card-title">Nombre billet Vendu</h4>
              
                <div class="table-responsive">
                    <table class="table table-striped">
                    <thead>
                        <tr>
                        
                        <th>
                            pack
                        </th>
                        <th>
                            nombre
                        </th>
                       
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($listeBilletVendu as $billet)
                            <tr>
                                    <td>
                                        {{$billet->nom_pack}}
                                    </td>
                                    
                                    <td>
                                        {{$billet->nombre}}
                                    </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
