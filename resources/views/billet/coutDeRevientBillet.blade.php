@extends('welcome')

@section('content')

<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                <h4 class="card-title">Cout de revient</h4>
              
                <div class="table-responsive">
                    <table class="table table-striped">
                    <thead>
                        <tr>
                        
                        <th>
                            pack
                        </th>
                        <th>
                            montant
                        </th>
                       
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($liste as $liste)
                            <tr>
                                    <td>
                                        {{$liste->nom_pack}}
                                    </td>
                                    
                                    <td>
                                        {{$liste->montant}}
                                    </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
