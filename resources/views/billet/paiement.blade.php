@extends('welcome')

@section('content')

<div class="content-wrapper">
    <div class="row">
        <div class="col-md-10 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                <h4 class="card-title">Paiement</h4>

                <form class="forms-sample" action="{{route('paiement.doPaiement')}}" method="post">
                    @csrf
                    @error("error")
                      {{$message}}
                    @enderror
                    <div class="form-group">
                        <label for="exampleInputEmail1">Numero</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="numero" name="numero" value="{{old('numero')}}">
                        @error("nombre")
                            {{$message}}
                        @enderror
                    </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Client </label>
                    <select class="form-control" id="exampleInputEmail1" name="client">
                        @foreach($listeClient as $client)
                            <option value="{{$client->id}}">{{$client->nom}}</option>
                        @endforeach
                    </select>
                    @error("lieu")
                      {{$message}}
                    @enderror
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Montant </label>
                    <input type="number" class="form-control" id="exampleInputEmail1" placeholder="nombre" name="montant" value="{{old('montant')}}">
                    @error("nombre")
                      {{$message}}
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary me-2">Valider</button>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>

    
@endsection