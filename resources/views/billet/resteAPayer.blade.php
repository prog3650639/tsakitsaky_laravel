@extends('welcome')

@section('content')

<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                <h4 class="card-title">Nombre billet Vendu</h4>
              
                <div class="table-responsive">
                    <table class="table table-striped">
                    <thead>
                        <tr>
                        
                        <th>
                            billet
                        </th>
                        <th>
                            pack
                        </th>
                        <th>
                            reste à payer
                        </th>
                       
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($listeBillet as $billet)
                            <tr>
                                    <td>
                                        {{$billet->id}}
                                    </td>
                                    <td>
                                        {{$billet->nom_pack}}
                                    </td>
                                    
                                    <td>
                                        {{$billet->reste_a_payer}}
                                    </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
