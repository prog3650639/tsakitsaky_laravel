@extends('welcome')

@section('content')

<div class="content-wrapper">
    <div class="row">
        <div class="col-md-10 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                <h4 class="card-title">Vente billet</h4>

                <form class="forms-sample" action="{{route('billet.vente')}}" method="get">
                    @error("error")
                      {{$message}}
                    @enderror
                    <div class="form-group">
                    <label for="exampleInputPassword1">Pack</label>
                    <select class="form-control" id="exampleInputEmail1" name="pack">
                        @foreach($listePack as $pack)
                            <option value="{{$pack->id}}">{{$pack->nom_pack}}</option>
                        @endforeach
                    </select>
                    @error("pack")
                      {{$message}}
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Lieu </label>
                    <select class="form-control" id="exampleInputEmail1" name="lieu">
                        @foreach($listeLieu as $lieu)
                            <option value="{{$lieu->id}}">{{$lieu->nom_lieu}}</option>
                        @endforeach
                    </select>
                    @error("lieu")
                      {{$message}}
                    @enderror
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Nombre</label>
                    <input type="number" class="form-control" id="exampleInputEmail1" placeholder="nombre" name="nombre" value="{{old('nombre')}}">
                    @error("nombre")
                      {{$message}}
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary me-2">Valider</button>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>

    
@endsection