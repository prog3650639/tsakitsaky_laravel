@extends('welcome')

@section('content')

<div class="content-wrapper">
    <div class="row">
        <div class="col-md-10 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                <h4 class="card-title">Pack</h4>

                <form class="forms-sample" action="{{route('pack.createPack')}}" method="get">
                @error("error")
                      {{$message}}
                    @enderror
                <div class="form-group">
                    <label for="exampleInputUsername1">Nom </label>
                    <input type="text" class="form-control" id="exampleInputUsername1" placeholder="nom" name="nom_pack" value="{{old('nom_pack')}}">
                    @error("nom_pack")
                      {{$message}}
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Prix</label>
                    <input type="number" class="form-control" id="exampleInputEmail1" placeholder="prix" name="prix_pack" value="{{old('prix_pack')}}">
                    @error("prix_pack")
                      {{$message}}
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary me-2">Valider</button>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>

    
@endsection