@extends('welcome')

@section('content')

<div class="content-wrapper">
    <div class="row">
        <div class="col-md-10 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                <h4 class="card-title">Formule</h4>

                <form class="forms-sample" action="{{route('pack.createFormulePack')}}" method="get">
                @error("error")
                      {{$message}}
                    @enderror
                <div class="form-group">
                    <label for="exampleInputPassword1">Pack</label>
                    <select class="form-control" id="exampleInputEmail1" name="pack">
                        @foreach($listePack as $pack)
                            <option value="{{$pack->id}}">{{$pack->nom_pack}}</option>
                        @endforeach
                    </select>
                    @error("pack")
                      {{$message}}
                    @enderror
                </div>
                <div class="inputs">
                    <div class="row" id="clone">
                        <div class="col">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Produit</label>
                                <select class="form-control" id="exampleInputEmail1" name="produits[0]">
                                    @foreach($listeproduit as $produit)
                                        <option value="{{$produit->id}}">{{$produit->nom_produit}}</option>
                                    @endforeach
                                </select>
                                @error("produits")
                                {{$message}}
                                @enderror
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Quantité</label>
                                <input type="number" class="form-control" id="exampleInputEmail1" placeholder="quantité" name="quantite[0]" value="{{old('quantite')}}">
                                @error("quantite")
                                {{$message}}
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="template-demo">
                    <button type="button" class="btn btn-outline-secondary btn-sm btnAjout">Ajouter</button>
                    <button type="submit" class="btn btn-primary me-2">Valider</button>
                </div>

                </form>
            </div>
            </div>
        </div>
    </div>
</div>


<script>
        let destination = document.querySelector('.inputs');
        let btn = document.querySelector('.btnAjout');
        btn.addEventListener('click',addChamps);
        let inputAjout=document.getElementById('clone');
        let i =0;

        function addChamps(e){
            i++;
            e.preventDefault();
            let newInput=inputAjout.cloneNode(true);
            newInput.value="";
            let selectElement = newInput.querySelector('select');
            let inputElement = newInput.querySelector('input');

            selectElement.name="produits["+i+"]";
            inputElement.name="quantite["+i+"]";
            destination.appendChild(newInput);
        }
    </script>
@endsection