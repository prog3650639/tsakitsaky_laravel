<?php

use App\Http\Controllers\BilletController;
use App\Http\Controllers\EtudiantController;
use App\Http\Controllers\PackController;
use App\Http\Controllers\PackProduitController;
use App\Http\Controllers\PaiementController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('authentification.login');
});


//etudiant
    Route::post('/log', [EtudiantController::class, 'login'])->name('etudiant.login');
    Route::get('/newUser', [EtudiantController::class, 'createEtudiant'])->name('etudiant.createEtudiant');
    Route::delete('/logOut', [EtudiantController::class, 'logOut'])->name('etudiant.logOut');

//billet
    Route::get('/DoVenteBillet', [BilletController::class, 'venteBillet'])->name('billet.vente')->middleware('etudiant');
    Route::get('/venteBillet', [BilletController::class, 'goToVenteBillet'])->name('billet.toVente')->middleware('etudiant');
    Route::get('/billetVendu', [BilletController::class, 'billetVendu'])->name('billet.billetVendu')->middleware('etudiant');
    Route::get('/coutDeRevient', [BilletController::class, 'coutDeRevientBillet'])->name('billet.coutDeRevient')->middleware('etudiant');

//pack
    Route::get('/creationPack', [PackController::class, 'createPack'])->name('pack.createPack')->middleware('etudiant');
    Route::get('/goToCreationPack', [PackController::class, 'goToCreationPack'])->name('pack.goToCreationPack')->middleware('etudiant');


//packProduit
    Route::get('/formulePack', [PackProduitController::class, 'gotToCreateFormule'])->name('pack.formulePack')->middleware('etudiant');
    Route::get('/createFormule', [PackProduitController::class, 'createFormulePack'])->name('pack.createFormulePack')->middleware('etudiant');
    
//paiement
    Route::get('/paiement', [PaiementController::class, 'goToPaiement'])->name('paiement.toPaiement')->middleware('etudiant');
    Route::post('/doPaiement', [PaiementController::class, 'doPaiement'])->name('paiement.doPaiement')->middleware('etudiant');
    Route::get('/etatPaiement', [PaiementController::class, 'etatPaiementBillet'])->name('paiement.etatPaiement')->middleware('etudiant');
    Route::get('/reste', [PaiementController::class, 'billetRestantPayer'])->name('paiement.reste')->middleware('etudiant');
   